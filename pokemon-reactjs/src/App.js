import React from 'react';
import './style/style.css';
import AppRoute from './components/Routes/AppRoute';
import Navbar from './components/Navbar';

const App = () => {
  return (
    <div className='App'>
      <Navbar />
      <AppRoute />
    </div>
  );
};

export default App;
