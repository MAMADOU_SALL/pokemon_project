import React, { useState, useEffect } from 'react';
import { ListPokemon, View } from './Pokemon-Element';
import '../style/localStorage.css';

const getDataStorage = () => {
  const data = localStorage.getItem('pokemons');
  if (data) {
    return JSON.parse(data);
  } else {
    return [];
  }
};

export const CreatePokemon = () => {
  const [pokemons, setPokemons] = useState(getDataStorage());

  const [id, setId] = useState('');
  const [name, setName] = useState('');
  const [image, setImage] = useState('');
  const [type, setType] = useState('');
  const [hp, setHp] = useState('');
  const [attack, setAttack] = useState('');
  const [defense, setDefense] = useState('');
  const [specialAttack, setSpecialAttack] = useState('');
  const [specialDefense, setSpecialDefense] = useState('');
  const [speed, setSpeed] = useState('');

  const handleAddPokemonSubmit = (e) => {
    e.preventDefault();

    let pokemon = {
      id,
      name,
      image,
      type,
      hp,
      attack,
      defense,
      specialAttack,
      specialDefense,
      speed,
    };
    setPokemons([...pokemons, pokemon]);
    setName('');
    setImage('');
    setType('');
    setHp('');
    setAttack('');
    setDefense('');
    setSpecialAttack('');
    setSpecialDefense('');
    setSpeed('');
  };

  const deletePokemon = (id) => {
    const filteredPokemons = pokemons.filter((element, index) => {
      return element.id !== id;
    });
    setPokemons(filteredPokemons);
  };

   function editData(rid) {
     const id = rid;
     let arr = getDataStorage();
     document.getElementById('id').value = arr[id];
   }

  useEffect(() => {
    localStorage.setItem('pokemons', JSON.stringify(pokemons));
  }, [pokemons]);

  return (
    <div className='wrapper'>
      <h1>Créer vos propre pokemon</h1>
      <div className='table-style'>
        <div class='form-container'>
          <form
            class=''
            autoComplete='off'
            className='form-group'
            onSubmit={handleAddPokemonSubmit}>
            <input
              id='id'
              class='form-field'
              type='text'
              placeholder='id pokemon'
              name='id'
              onChange={(e) => setId(e.target.value)}
              value={id}></input>
            <input
              id='name'
              class='form-field'
              type='text'
              placeholder='Nom du pokemon'
              name='name'
              required
              onChange={(e) => setName(e.target.value)}
              value={name}></input>
               <input
              id='image'
              class='form-field'
              type='file'
              placeholder='Imagedu pokemon'
              name='Image'
            
              onChange={(e) => setImage(e.target.value)}
              value={image}></input>
            <input
              id='type'
              class='form-field'
              type='text'
              placeholder='type du pokemon'
              name='type'
              required
              onChange={(e) => setType(e.target.value)}
              value={type}></input>

            <input
              id='hp'
              class='form-field'
              type='text'
              placeholder='hp du pokemon'
              name='hp'
              onChange={(e) => setHp(e.target.value)}
              value={hp}></input>
            <input
              id='attack'
              class='form-field'
              type='text'
              placeholder='Attaque du pokemon'
              name='attack'
              onChange={(e) => setAttack(e.target.value)}
              value={attack}></input>

            <input
              id='defense'
              class='form-field'
              type='text'
              placeholder='Defense du pokemon'
              name='defense'
              onChange={(e) => setDefense(e.target.value)}
              value={defense}></input>
            <input
              id='special-attack'
              class='form-field'
              type='text'
              placeholder='Attaque special du pokemon'
              name='specialAttack'
              onChange={(e) => setSpecialAttack(e.target.value)}
              value={specialAttack}></input>
            <input
              id='special-defense'
              class='form-field'
              type='text'
              placeholder='Defense special du pokemon'
              name='specialDefense'
              onChange={(e) => setSpecialDefense(e.target.value)}
              value={specialDefense}></input>
            <input
              id='speed'
              class='form-field'
              type='text'
              placeholder='Speed du pokemon'
              name='speed'
              onChange={(e) => setSpeed(e.target.value)}
              value={speed}></input>

            <button type='submit' className='btn btn-success btn-md'>
              Ajouter
            </button>
            <button type='submit' className='btn btn-success btn-md' o onClick={() => editData(this.id)}>
              Modifier
            </button>
          </form>
        </div>
        <div className='view-container'>
          <h1>Liste des pokemons créés</h1>
          {pokemons.length > 0 && (
            <>
              <div className='table-responsive'>
                <table className='table'>
                  <thead>
                    <tr>
                      <th>#ID</th>
                      <th>Name</th>
                      <th>Image</th>
                      <th>Type</th>
                      <th>HP</th>
                      <th>Attaque</th>
                      <th>Defence</th>
                      <th>Special Att.</th>
                      <th>Special Def.</th>
                      <th>Speed</th>
                  
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    <ListPokemon
                      pokemons={pokemons}
                      deletePokemon={deletePokemon}
                    />
                  </tbody>
                </table>
              </div>
              <button
                className='btn btn-danger btn-md'
                onClick={() => setPokemons([])}>
                Supprimer tout
              </button>
            </>
          )}
          {pokemons.length < 1 && (
            <div align='center'>Pas de pokemon ajouter encore !</div>
          )}
        </div>
      </div>
    </div>
  );
};

export default CreatePokemon;
