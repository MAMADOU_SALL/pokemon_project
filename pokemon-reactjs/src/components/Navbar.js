import React from 'react';

const Navbar = () => {
  let imgUrl =
    'https://raw.githubusercontent.com/PokeAPI/media/master/logo/pokeapi_256.png';

  return (
    <nav>
      <div />
      <div>
        <a href='http://localhost:3000'>
          <img src={imgUrl} alt='Logo' className='navbar-image' />
        </a>
      </div>
    </nav>
  );
};

export default Navbar;
