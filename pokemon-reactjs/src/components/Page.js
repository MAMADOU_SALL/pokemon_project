import React from 'react';
import { LeftArrow, RightArrow } from './ArrowsSvg';

const Page = (props) => {
  const { onLeftClick, onRightClick, page, totalPages } = props;

  return (
    <div className='page'>
      <button className='page-btn' onClick={onLeftClick}>
        <div className='icon'>
          <LeftArrow />
        </div>
      </button>
      <div>
        {page} de {totalPages}
      </div>
      <button className='page-btn' onClick={onRightClick}>
        <div className='icon'>
          <RightArrow />
        </div>
      </button>
    </div>
  );
};

export default Page;
