import React from 'react';

export const ListPokemon = ({ pokemons, deletePokemon }) => {
  return pokemons.map((pokemon) => (
    <tr key={pokemon.id}>
      <td>{pokemon.id}</td>
      <td>{pokemon.name}</td>
      <td>{pokemon.image}</td>
     
      <td>{pokemon.type}</td>
      <td>{pokemon.hp}</td>
      <td>{pokemon.attack}</td>
      <td>{pokemon.defense}</td>
      <td>{pokemon.specialAttack}</td>
      <td>{pokemon.specialDefense}</td>
      <td>{pokemon.speed}</td>

      <td className='delete-btn' onClick={() => deletePokemon(pokemon.id)}>
        suprimer
      </td>
    </tr>
  ));
};
