import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from '../../pages/Home';

import PokemonPage from '../../pages/PokemonPage';
import CreatePokemon from '../Create-Pokemon';

const AppRoute = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/list-pokemon' element={<PokemonPage />} />
        <Route path='/create-pokemon' element={<CreatePokemon />} />
      </Routes>
    </BrowserRouter>
  );
};

export default AppRoute;
