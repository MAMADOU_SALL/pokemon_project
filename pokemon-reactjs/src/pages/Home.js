import React from 'react';
import '../style/home.css';
import { useNavigate } from 'react-router-dom';

const Home = () => {
  const navigate = useNavigate();
  const btn1_Click = () => {
    navigate('/list-pokemon');
  };
  const btn2_Click = () => {
    navigate('/create-pokemon');
  };
  return (
    <div className='Application'>
      <div className='welcome'>
        <h1> Welcome To Pokemon Page</h1>
        <p></p>
      </div>
      <div className='text'>
        {' '}
        Cette page vous permet de découvrir les différentes pokemon mais aussi
        en créé.
      </div>
      <div className='btn'>
        <div className='btn-1'>
          <button onClick={btn1_Click}>
            <span>liste de pokemon</span>
          </button>
        </div>
        <div className='btn-2'>
          <button onClick={btn2_Click}>
            <span>Créer un pokemon</span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default Home;
